package controllers;

import actors.Setup;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.mvc.Controller;
import play.mvc.Result;
import scala.compat.java8.FutureConverters;
import play.libs.ws.*;

import java.util.concurrent.CompletionStage;
import static akka.pattern.Patterns.ask;

@Singleton
public class Application extends Controller {

    Setup actorSetup;

    WSClient wsClient;

    @Inject
    public Application(Setup actorSetup, WSClient wsClient) {
        this.actorSetup = actorSetup;
        this.wsClient = wsClient;
    }

    /**
     * Play framework suppors asynchronous controller methods -- that is, methods that instead of returning a Result
     * return a CompletionStage, which will produce a Result some time in the future
     */
    public CompletionStage<Result> index() {
        /*
         * This code might look a little complex.
         *
         * ask sends a message to an ActorRef, and then returns a Future that will eventually contain the response.
         * But Future is a Scala class, so FutureConverters.toJava converts it into a CompletionStage, which is Java's equivalent.
         * thenApply is a method on CompletionStage that means "when you get the result, do this with it, and return a new CompletionStage"
         */
        return FutureConverters.toJava(ask(actorSetup.marshallActor, "Report!", 1000))
                .thenApply(response -> ok(response.toString()));
    }

    /**
     * This controller method uses Play's Web Service client to make another HTTP call, and do something with the
     * response
     */
    public CompletionStage<Result> whatDidGitLabSay() {
        return wsClient.url("http://www.une.edu.au/foo").get().thenApply(r -> ok(r.getStatusText()));
    }
}
